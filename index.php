<?php
/**
 * Created by PhpStorm.
 * User: Andrey Katkov
 * Date: 19.03.2016
 * Time: 11:44
 */
date_default_timezone_set('Europe/Moscow');
$img_files = array(
    $title = array("Name", "Size", "Time of changed", "Links", "Preview"),
    $name = array(),
    $size = array(),
    $mtime = array()
);
for ($i = 1; ; $i++) {
    $file = 'img/'.$i.'.jpg';
    $file_preview = 'img/'.$i.'_small.jpg';
    $filename = $i.'.jpg';
    $percent = 0.1;
    header('Content-Type: image/jpeg');
    list($width, $height) = getimagesize($file);
    $new_width = $width * $percent;
    $new_height = $height * $percent;
    $image_p = imagecreatetruecolor($new_width, $new_height);
    $image = imagecreatefromjpeg($file);
    imagecopyresampled($image_p, $image, 0, 0, 0, 0, $new_width, $new_height, $width, $height);
    imagejpeg($image_p, $file_preview, 100);
    if (file_exists($file) !== false) {
        $stat = stat('img/'.$i.'.jpg');
        $img_files[$i][0] = $filename;
        $img_files[$i][1] = round($stat['size']/1024)." Kbyte";
        $img_files[$i][2] = date("Y-m-d H:i:s", $stat['mtime']);
        $img_files[$i][3] = "<a href=".$file." target="."_blank"."><ins>link</ins></a>";
        $img_files[$i][4] = "<a href=".$file." target="."_blank"."><img src=".$file_preview."></a>";
    } else {
        break;
    }
}
$csv = fopen('img.csv', 'w');
foreach ($img_files as $fields) {
    fputcsv($csv, $fields, $delimiter = "; ");
}
fclose($csv);
